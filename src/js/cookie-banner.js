class CookieBanner extends HTMLElement {
    shadowRoot;

    constructor() {
        super();

        let settings = this.getSettings();

        const cookieAccepted = this.getCookie('cookie.banner.accepted');
        let bannerState = '';

        if (cookieAccepted !== null) {
            bannerState = 'hidden';
        }

        this.shadowRoot = this.attachShadow({mode: 'closed'});

        const style = document.createElement('style');
        let stylesContent = `
            .hidden{
            display:none;
            }
            .accept-all{
            background-color:green;
            color:white;
            }
            button:hover{
            cursor:pointer;
            }
        `;

        if (settings.additionalStyles !== '') {
            stylesContent += settings.additionalStyles;
        }
        style.textContent = stylesContent;

        this.shadowRoot.appendChild(style.cloneNode(true));

        const template = document.createElement('template');

        let innerHtml = '';

        innerHtml += `
        <div class="cookie-banner ${bannerState}">
            <div class="cookie-banner-inner">
                <h3>${settings.title}</h3>
                <p>${settings.text}</p>
                <p class="cookie-sources">`;

        if (settings.useRequired || settings.additionalCookies.length === 0) {
            innerHtml += `
                    <label><input type="checkbox" name="required" disabled checked>${settings.requiredName}</label>`;
        }

        let cb = this;
        settings.additionalCookies.forEach(function(cookie){
            let cleanCookieName = cb.cleanString(cookie);
            innerHtml += `
                    <label><input type="checkbox" name="${cleanCookieName}">${cookie}</label>`;
        });

        innerHtml += `
                </p>
                <button class="accept">${settings.acceptSelectedText}</button>
                <button class="accept-all">${settings.acceptAllText}</button>
            </div>
        </div>
        `;

        template.innerHTML = innerHtml;

        this.shadowRoot.appendChild(template.content.cloneNode(true));
    }

    connectedCallback() {
        const element = this;
        this.shadowRoot.querySelector('button.accept').addEventListener('click', function (event) {
            element.setCookie('cookie.banner.accepted', true, 30);

            element.shadowRoot.querySelectorAll('.cookie-sources input[type="checkbox"]:checked').forEach(function (source) {
                element.setCookie(`cookie.accept.${source.name}`, true, 30);
            });
            element.hideCookieBanner();
        });
        this.shadowRoot.querySelector('button.accept-all').addEventListener('click', function (event) {
            element.setCookie('cookie.banner.all', true, 30);
            element.setCookie('cookie.banner.accepted', true, 30);
            element.hideCookieBanner();
        });
    }

    hideCookieBanner() {
        this.shadowRoot.querySelector('.cookie-banner').classList.add('hidden');
    }

    setCookie(name, value, days) {
        let expires = "";
        if (days) {
            let date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }

    getCookie(name) {
        let nameEQ = name + "=";
        let ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    eraseCookie(name) {
        document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

    cleanString(str) {

        if (str) {

            let strLower = str.toLowerCase();
            strLower = strLower.replace(/\s/g, '');

            return strLower.replace(/\W/g, '');

        }

        return false;

    }

    getSettings() {

        let settings;

        let options = (typeof cookieBannerOptions === 'undefined') ? {} : cookieBannerOptions;

        settings = this.extend({
            title              : 'Accept Cookies',          /** String: Used as Banner Title */
            text               : 'Cookies are always used', /** String: Used as Banner Text, i.e. description */
            requiredName       : 'Required',                /** String: Name for the "Required" cookie */
            useRequired        : true,                      /** Boolean: Use "Required" cookie, if no additional cookies set, this will be always true */
            additionalCookies  : [],                        /** Array: Containing the Names for additional Cookies, if empty "Required" will always be shown */
            acceptAllText      : 'Accept All',              /** String: Text for the "accept all" button */
            acceptSelectedText : 'Accept Selected',         /** String: Text for the "accept selected" button */
            additionalStyles   : '',                        /** String: Additional Styles */
        }, options);

        return settings;
    }

    extend( defaults, options ) {
        let extended = {};
        let prop;
        for (prop in defaults) {
            if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
                extended[prop] = defaults[prop];
            }
        }
        for (prop in options) {
            if (Object.prototype.hasOwnProperty.call(options, prop)) {
                extended[prop] = options[prop];
            }
        }
        return extended;
    };
}

customElements.define('cookie-banner', CookieBanner);
