# Cookie Banner

> Based on https://gist.github.com/BlackScorp/d7390fead02103c9ab4ea174a8a9b9c0  
Thanks to BlackScorp :-)

This JavaScript can be used, to show a Cookie Banner regarding the [ePrivacy Directive](https://edps.europa.eu/data-protection/our-work/subjects/eprivacy-directive_en) since 2002.

## Usage

Simply add ``<cookie-banner></cookie-banner>`` html tag inside ``body`` in your HTML Document and include the ``cookie-banner.js``;

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Web Component - Cookie Banner</title>
</head>
<body>

<cookie-banner></cookie-banner>
<script>
  /** Optional Settings */
  const cookieBannerOptions = {
    title             : 'This is the Project Title',
    text              : 'This is the special Text',
    additionalCookies : ['Statistics', 'Marketing', 'Foo', 'Bar']
  };
</script>
<script defer src="js/cookie-banner.js"></script>
</body>
</html>
```

## Optional Settings

Can be optionally set before including the ``cookie-banner.js`` into your document.

_See ``Usage`` example before._

| Name | Type | Default | Description |
|---|---|---|---|
| ``title`` | String | ``Accept Cookies`` | Used as Banner Title |
| ``text`` | String | ``Cookies are always used`` | Used as Banner Text, i.e. description |
| ``requiredName`` | Boolean | ``Required`` | Use "Required" cookie (if no additional cookies set, this will be always ``true``) |
| ``additionalCookies`` | Array | ``[]`` | Containing the Names for additional Cookies (if empty "Required" cookie will always be shown) |
| ``acceptAllText`` | String | ``Accept All`` | Text for the "accept all" button |
| ``acceptSelectedText`` | String | ``Accept Selected`` | Text for the "accept selected" button |
| ``additionalStyles`` | String | ``''`` | Additional Styles |
